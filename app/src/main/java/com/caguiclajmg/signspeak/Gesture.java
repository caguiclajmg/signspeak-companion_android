package com.caguiclajmg.signspeak;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by caguicla.jmg on 15/01/2018.
 */

@Entity
class Gesture {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "title")
    private String _title;

    String getTitle() {
        return _title;
    }

    void setTitle(String title) {
        _title = title;
    }

    @ColumnInfo(name = "description")
    private String _description;

    String getDescription() {
        return _description;
    }

    void setDescription(String description) {
        _description = description;
    }

    @ColumnInfo(name = "flex0")
    private int _flex0;

    int getFlex0() {
        return _flex0;
    }

    void setFlex0(int flex0) {
        _flex0 = flex0;
    }

    @ColumnInfo(name = "flex1")
    private int _flex1;

    int getFlex1() {
        return _flex1;
    }

    void setFlex1(int flex1) {
        _flex1 = flex1;
    }

    @ColumnInfo(name = "flex2")
    private int _flex2;

    int getFlex2() {
        return _flex2;
    }

    void setFlex2(int flex2) {
        _flex2 = flex2;
    }

    @ColumnInfo(name = "flex3")
    private int _flex3;

    int getFlex3() {
        return _flex3;
    }

    void setFlex3(int flex3) {
        _flex3 = flex3;
    }

    @ColumnInfo(name = "flex4")
    private int _flex4;

    int getFlex4() {
        return _flex4;
    }

    void setFlex4(int flex4) {
        _flex4 = flex4;
    }

    @ColumnInfo(name = "flex5")
    private int _flex5;

    int getFlex5() {
        return _flex5;
    }

    void setFlex5(int flex5) {
        _flex5 = flex5;
    }

    @ColumnInfo(name = "flex6")
    private int _flex6;

    int getFlex6() {
        return _flex6;
    }

    void setFlex6(int flex6) {
        _flex6 = flex6;
    }

    @ColumnInfo(name = "flex7")
    private int _flex7;

    int getFlex7() {
        return _flex7;
    }

    void setFlex7(int flex7) {
        _flex7 = flex7;
    }

    @ColumnInfo(name = "flex8")
    private int _flex8;

    int getFlex8() {
        return _flex8;
    }

    void setFlex8(int flex8) {
        _flex8 = flex8;
    }
    
    @ColumnInfo(name = "flex9")
    private int _flex9;

    int getFlex9() {
        return _flex9;
    }

    void setFlex9(int flex9) {
        _flex9 = flex9;
    }

    @ColumnInfo(name = "rotationX")
    private float _rotationX;

    float getRotationX() {
        return _rotationX;
    }

    void setRotationX(float rotationX) {
        _rotationX = rotationX;
    }

    @ColumnInfo(name = "rotationY")
    private float _rotationY;

    float getRotationY() {
        return _rotationY;
    }

    void setRotationY(float rotationY) {
        _rotationY = rotationY;
    }

    @ColumnInfo(name = "rotationZ")
    private float _rotationZ;

    float getRotationZ() {
        return _rotationZ;
    }

    void setRotationZ(float rotationZ) {
        _rotationZ = rotationZ;
    }

    Gesture() {

    }

    Gesture(String title, String description, int[] flex, float rotation[]) {
        setTitle(title);
        setDescription(description);
        setFlex0(flex[0]);
        setFlex1(flex[1]);
        setFlex2(flex[2]);
        setFlex3(flex[3]);
        setFlex4(flex[4]);
        setFlex5(flex[5]);
        setFlex6(flex[6]);
        setFlex7(flex[7]);
        setFlex8(flex[8]);
        setFlex9(flex[9]);
        setRotationX(rotation[0]);
        setRotationX(rotation[1]);
        setRotationX(rotation[2]);
    }
}
