package com.caguiclajmg.signspeak;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by caguicla.jmg on 15/01/2018.
 */

@Database(entities = {Gesture.class}, version = 1)
public abstract class GestureDatabase extends RoomDatabase {
    static GestureDatabase buildInstance(Context context) {
        if(_instance == null) _instance = Room.databaseBuilder(context, GestureDatabase.class, "gesture-database").build();

        return _instance;
    }

    public static GestureDatabase getInstance() {
        return _instance;
    }

    public abstract GestureDao gestureDao();
    private static GestureDatabase _instance = null;
}
