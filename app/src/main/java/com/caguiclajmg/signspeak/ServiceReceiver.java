package com.caguiclajmg.signspeak;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import org.androidannotations.annotations.EService;

/**
 * Created by caguicla.jmg on 28/01/2018.
 */

@EService
public class ServiceReceiver extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
