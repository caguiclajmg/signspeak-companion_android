package com.caguiclajmg.signspeak;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by caguicla.jmg on 15/01/2018.
 */

@Dao
public interface GestureDao {
    @Query("SELECT * FROM gesture")
    List<Gesture> getAll();

    @Query("SELECT * FROM gesture WHERE title LIKE :title LIMIT 1")
    Gesture findByTitle(String title);

    @Query("SELECT * FROM gesture WHERE description LIKE :description LIMIT 1")
    Gesture findByDescription(String description);

    @Query("SELECT * FROM gesture ORDER BY " +
            "ABS(flex0 - :flex0) + " +
            "ABS(flex1 - :flex1) + " +
            "ABS(flex2 - :flex2) + " +
            "ABS(flex3 - :flex3) + " +
            "ABS(flex4 - :flex4) LIMIT 1")
    Gesture findNearest5_FlexOnly(int flex0, int flex1, int flex2, int flex3, int flex4);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Gesture... gestures);

    @Delete
    void delete(Gesture gesture);

    @Query("DELETE FROM gesture")
    void truncate();
}
