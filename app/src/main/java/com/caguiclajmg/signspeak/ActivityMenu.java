package com.caguiclajmg.signspeak;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Locale;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import app.akexorcist.bluetotohspp.library.BluetoothState;

@EActivity
public class ActivityMenu extends AppCompatActivity {
    short _voltage;
    short _flex[] = new short[10];
    float _rotation[] = new float[3];
    short _acceleration[] = new short[3];

    TextToSpeech _tts;

    GestureDatabase _database = GestureDatabase.buildInstance(this);
    volatile Gesture _currentGesture = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_menu);

        BluetoothSPP bluetoothSPP = new BluetoothSPP(this);

        bluetoothSPP.setOnDataReceivedListener(new BluetoothSPP.OnDataReceivedListener() {
            @Override
            public void onDataReceived(byte[] data, String message) {
                if(data.length != 40) return;

                ByteBuffer byteBuffer = ByteBuffer.wrap(data);
                byteBuffer.order(ByteOrder.LITTLE_ENDIAN);

                StringBuilder stringBuilder = new StringBuilder();

                _voltage = byteBuffer.getShort();

                for(int i = 0; i < 10; ++i) _flex[i] = byteBuffer.getShort();

                for(int i = 0; i < 3; ++i) _rotation[i] = byteBuffer.getFloat();

                for(int i = 0; i < 3; ++i) _acceleration[i] = byteBuffer.getShort();
            }
        });

        bluetoothSPP.setupService();
        bluetoothSPP.startService(BluetoothState.DEVICE_OTHER);

        bluetoothSPP.connect("98:D3:31:F4:1E:86");
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                String display = "voltage: %d\n" +
                        "flex[0]: %d\n" +
                        "flex[1]: %d\n" +
                        "flex[2]: %d\n" +
                        "flex[3]: %d\n" +
                        "flex[4]: %d\n" +
                        "flex[5]: %d\n" +
                        "flex[6]: %d\n" +
                        "flex[7]: %d\n" +
                        "flex[8]: %d\n" +
                        "flex[9]: %d\n" +
                        "rotation[0]: %f\n" +
                        "rotation[1]: %f\n" +
                        "rotation[2]: %f\n" +
                        "acceleration[0]: %d\n" +
                        "acceleration[1]: %d\n" +
                        "acceleration[2]: %d\n";

                //_tts.speak("お前はもう死んでいる", TextToSpeech.QUEUE_ADD, null);

                getGesture(_flex[0], _flex[1], _flex[2], _flex[3], _flex[4]);

                Log.d(this.getClass().getName(), String.format(Locale.US, "%d %d %d %d %d", _flex[0], _flex[1], _flex[2], _flex[3], _flex[4] ));
                ((TextView) findViewById(R.id.lblF0)).getLayoutParams().height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (((float) _flex[0] - 700.f) / 200.f) * 128.f, getResources().getDisplayMetrics());
                ((TextView) findViewById(R.id.lblF0)).forceLayout();
                ((TextView) findViewById(R.id.lblF1)).getLayoutParams().height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (((float) _flex[1] - 700.f) / 200.f) * 128.f, getResources().getDisplayMetrics());
                ((TextView) findViewById(R.id.lblF1)).forceLayout();
                ((TextView) findViewById(R.id.lblF2)).getLayoutParams().height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (((float) _flex[2] - 700.f) / 200.f) * 128.f, getResources().getDisplayMetrics());
                ((TextView) findViewById(R.id.lblF2)).forceLayout();
                ((TextView) findViewById(R.id.lblF3)).getLayoutParams().height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (((float) _flex[3] - 700.f) / 200.f) * 128.f, getResources().getDisplayMetrics());
                ((TextView) findViewById(R.id.lblF3)).forceLayout();
                ((TextView) findViewById(R.id.lblF4)).getLayoutParams().height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (((float) _flex[4] - 700.f) / 200.f) * 128.f, getResources().getDisplayMetrics());
                ((TextView) findViewById(R.id.lblF4)).forceLayout();

                ((TextView) findViewById(R.id.lblF0)).requestLayout();

                ((TextView) findViewById(R.id.lblTitle)).setText(_currentGesture != null ? _currentGesture.getTitle() : "N/A");

                /*if(_currentGesture != null) {
                    _tts.speak(_currentGesture.getTitle(), TextToSpeech.QUEUE_FLUSH, null);
                }*/

                TextView textView = (TextView) findViewById(R.id.textView);
                //textView.setText(String.format(Locale.US, display, _voltage, _flex[0], _flex[1], _flex[2], _flex[3], _flex[4], _flex[5], _flex[6], _flex[7], _flex[8], _flex[9], _rotation[0], _rotation[1], _rotation[2], _acceleration[0], _acceleration[1], _acceleration[2]));

                handler.postDelayed(this, 500);
            }
        });

        _tts = new TextToSpeech(ActivityMenu.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if(i == TextToSpeech.SUCCESS) {
                    _tts.setLanguage(Locale.ENGLISH);

                    for (TextToSpeech.EngineInfo engine: _tts.getEngines()) {
                        Log.e(this.getClass().getName(), engine.name);
                    }
                }
            }
        });

        ((Button) findViewById(R.id.btnSave)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText input = new EditText(ActivityMenu.this);
                input.setInputType(InputType.TYPE_CLASS_TEXT);

                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityMenu.this);
                builder.setView(input);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SaveGesture(input.getText().toString(), "", _flex[0], _flex[1], _flex[2], _flex[3], _flex[4]);
                    }
                });

                builder.show();
            }
        });

        ((Button) findViewById(R.id.btnSpeak)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _tts.speak(_currentGesture != null ? _currentGesture.getTitle() : "", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
    }

    @Background
    void getGesture(int flex0, int flex1, int flex2, int flex3, int flex4) {
        _currentGesture = _database.gestureDao().findNearest5_FlexOnly(flex0, flex1, flex2, flex3, flex4);
    }

    @Click(R.id.btnDelete)
    void btnDelete_Click() {
        clearGesture();
    }

    @Background
    void clearGesture() {
        _database.gestureDao().truncate();
    }

    void SaveGesture(String title, String description, int flex0, int flex1, int flex2, int flex3, int flex4) {
        final Gesture gesture = new Gesture();
        gesture.setTitle(title);
        gesture.setDescription(description);
        gesture.setFlex0(flex0);
        gesture.setFlex1(flex1);
        gesture.setFlex2(flex2);
        gesture.setFlex3(flex3);
        gesture.setFlex4(flex4);

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                _database.gestureDao().insertAll(gesture);
            }
        });
    }
}
